function isOther(value) {
    return value != this;
}

function notInArray(value) {
    return !this.includes(value);
}

function SudokuField(x, y, parent) {
    this.value = null
    this.possibleValues = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    this.x = x
    this.y = y
    this.parent = parent
}

SudokuField.prototype.setValue = function(value) {
    if (this.possibleValues.includes(value)) {
        this.value = value
        this.possibleValues = []
        this.parent.notifyChanges(this.x, this.y)
    }
}

SudokuField.prototype.removePossible = function(value) {
    this.possibleValues = this.possibleValues.filter(isOther, value);
}

function Sudoku() {
    // initialize empty field
    this.sudoku = new Array(9)
    for (var i = 0; i < 9; i++) { 
        this.sudoku[i] = new Array(9)
    }

    for (var i = 0; i < 9; i++) { 
        for (var j = 0; j < 9; j++) { 
            this.sudoku[i][j] = new SudokuField(i, j, this)
        }
    }
}

Sudoku.prototype.notifyChanges = function(x, y) {
    var value = this.sudoku[x][y].value

    var baseX = Math.floor(x / 3) * 3;
    var baseY = Math.floor(y / 3) * 3;

    /* remove possibles from little quadrant */
    for (var i = baseX; i < baseX + 3; i++){
        for (var j = baseY; j < baseY + 3; j++){
            this.sudoku[i][j].removePossible(value)
        }
    }

    /* remove possibles from col and row */
    for (var i = 0; i < 9; i ++){
        this.sudoku[i][y].removePossible(value);
        this.sudoku[x][i].removePossible(value);
    }

    this.updateAllOnlyPossible();

    this.setValuesAccordingToPossible();
}

Sudoku.prototype.updateAllOnlyPossible = function() {
    for (var i = 0; i < 9; i++) { 
        for (var j = 0; j < 9; j++) { 
            this.updateOnlyPossible(i, j)
        }
    }
}

Sudoku.prototype.updateOnlyPossible = function(x, y) {
    var allPossibles = []

    for (var i = 0; i < 9; i++) { 
        if (i != x) {
            allPossibles = allPossibles.concat(this.sudoku[i][y].possibleValues)
        }
    }
    onlyPossibleHere = this.sudoku[x][y].possibleValues.filter(notInArray, allPossibles);
    if (onlyPossibleHere.length > 0) this.sudoku[x][y].possibleValues = onlyPossibleHere;

    allPossibles = []
    for (var i = 0; i < 9; i++) { 
       if (i != y) {
            allPossibles = allPossibles.concat(this.sudoku[x][i].possibleValues)
        }
    }
    onlyPossibleHere = this.sudoku[x][y].possibleValues.filter(notInArray, allPossibles);
    if (onlyPossibleHere.length > 0) this.sudoku[x][y].possibleValues = onlyPossibleHere;

    allPossibles = []
    var baseX = Math.floor(x / 3) * 3;
    var baseY = Math.floor(y / 3) * 3;

     for (var i = baseX; i < baseX + 3; i++){
        for (var j = baseY; j < baseY + 3; j++){
            if ((x !== i) || (y !== j)) {
                allPossibles = allPossibles.concat(this.sudoku[i][j].possibleValues)
            }                
        }
    }
    onlyPossibleHere = this.sudoku[x][y].possibleValues.filter(notInArray, allPossibles);
    if (onlyPossibleHere.length > 0) this.sudoku[x][y].possibleValues = onlyPossibleHere;
}

Sudoku.prototype.setValuesAccordingToPossible = function() {
    for (var i = 0; i < 9; i++) { 
        for (var j = 0; j < 9; j++) { 
            if (this.sudoku[i][j].possibleValues.length == 1) {
                this.sudoku[i][j].setValue(this.sudoku[i][j].possibleValues[0])        
            }
        }
    }
}

Sudoku.prototype.visualize = function() {
    for (var i = 0; i < 9; i++) { 
        var text = ""
        for (var j = 0; j < 9; j++) { 
            if (this.sudoku[i][j].value != null)
                text += this.sudoku[i][j].value + " "
            else
                text += "# "
        }
        console.log(text)
    }
}


/* Erzeuge leichtes Sudoku */
sudoku = new Sudoku()

sudoku.sudoku[0][2].setValue(3)
sudoku.sudoku[0][3].setValue(9)
sudoku.sudoku[0][5].setValue(4)
sudoku.sudoku[0][7].setValue(6)
sudoku.sudoku[1][1].setValue(1)
sudoku.sudoku[1][8].setValue(7)
sudoku.sudoku[2][1].setValue(9)
sudoku.sudoku[2][2].setValue(5)
sudoku.sudoku[3][3].setValue(8)
sudoku.sudoku[3][5].setValue(7)
sudoku.sudoku[3][7].setValue(2)
sudoku.sudoku[3][8].setValue(5)
sudoku.sudoku[4][3].setValue(5)
sudoku.sudoku[4][5].setValue(9)
sudoku.sudoku[5][0].setValue(7)
sudoku.sudoku[5][1].setValue(5)
sudoku.sudoku[5][3].setValue(3)
sudoku.sudoku[5][5].setValue(6)
sudoku.sudoku[6][6].setValue(4)
sudoku.sudoku[6][7].setValue(9)
sudoku.sudoku[7][0].setValue(5)
sudoku.sudoku[7][7].setValue(3)
sudoku.sudoku[8][1].setValue(6)
sudoku.sudoku[8][3].setValue(7)
sudoku.sudoku[8][5].setValue(1)
sudoku.sudoku[8][6].setValue(5)

sudoku.visualize()


/* Erzeuge schweres Sudoku */
sudoku = new Sudoku()

sudoku.sudoku[0][1].setValue(2)
sudoku.sudoku[0][3].setValue(1)
sudoku.sudoku[0][7].setValue(3)
sudoku.sudoku[1][0].setValue(9)
sudoku.sudoku[1][3].setValue(5)
sudoku.sudoku[1][6].setValue(1)
sudoku.sudoku[2][0].setValue(6)
sudoku.sudoku[2][3].setValue(3)
sudoku.sudoku[2][4].setValue(2)
sudoku.sudoku[3][0].setValue(1)
sudoku.sudoku[3][2].setValue(9)
sudoku.sudoku[3][8].setValue(7)
sudoku.sudoku[4][2].setValue(2)
sudoku.sudoku[4][6].setValue(8)
sudoku.sudoku[5][0].setValue(4)
sudoku.sudoku[5][6].setValue(9)
sudoku.sudoku[5][8].setValue(6)
sudoku.sudoku[6][4].setValue(1)
sudoku.sudoku[6][5].setValue(3)
sudoku.sudoku[6][8].setValue(4)
sudoku.sudoku[7][2].setValue(6)
sudoku.sudoku[7][5].setValue(4)
sudoku.sudoku[7][8].setValue(2)
sudoku.sudoku[8][1].setValue(9)
sudoku.sudoku[8][5].setValue(7)
sudoku.sudoku[8][7].setValue(6)

sudoku.visualize()